var arrNum = [];

//handle Thêm số
function themSo() {
  var number = document.querySelector("#txt-number").value * 1;
  arrNum.push(number);
  document.querySelector("#txt-number").value = "";
  document.querySelector("#Them-so").innerHTML = `<h3> ${arrNum}</h3>`;
}

//1.Handle Tính tổng
function handleTong() {
  document.getElementById(
    "result-1"
  ).innerHTML = `<h3>Tổng số dương : ${tinhTong(arrNum)}</h1>`;
}
function tinhTong(arr) {
  var sum = 0;
  arr.forEach(function (number) {
    if (number > 0) {
      sum += number;
    }
  });
  return sum;
}

//2.Handle Đếm số dương
function handleDemSo() {
  document.getElementById("result-2").innerHTML = `<h3>Số dương : ${demSoDuong(
    arrNum
  )}</h3>`;
}
function demSoDuong(arr) {
  var demSoDuong = 0;
  for (var index = 0; index < arr.length; index++) {
    var number = arr[index];
    if (number > 0) {
      demSoDuong++;
    }
  }
  return demSoDuong;
}

//3.Handle Tìm số nhỏ nhất
function handleSoNhoNhat() {
  var minSo = arrNum[0];
  for (var index = 0; index < arrNum.length; index++) {
    if (arrNum[index] < minSo) {
      minSo = arrNum[index];
    }
  }
  document.querySelector(
    "#result-3"
  ).innerHTML = `<h3>Số Nhỏ Nhất : ${minSo}</h3>`;
}

// 4.Handle Số dương nhỏ nhất
function handleSoDuongNhoNhat() {
  document.querySelector("#result-4").innerHTML=`<h3> Số dương nhỏ nhất : ${soDuongNhoNhat(arrNum)}</h3>`
}
function soDuongNhoNhat(arr) {
  var soDuongNhoNhat = -1;
  for (var i = 0 ; i < arr.length;i++) {
    if (soDuongNhoNhat == -1 || (arr[i] < soDuongNhoNhat && arr[i] > 0)) {
      soDuongNhoNhat = arr[i];
    }
  }
  return soDuongNhoNhat;
}

//5.Handle Tìm số chẵn cuối cùng
function handleSoChanCuoiCung() { 
  document.getElementById(
    "result-5"
  ).innerHTML = `<h3>Số chẵn cuối cùng : ${soChanCuoiCung(arrNum)}</h3>`;
}

function soChanCuoiCung(arr) {
  soChanCuoiCung = 0;
  for (var index = 0; index < arr.length; index++) {
    if (arr[index] % 2 == 0) {
      soChanCuoiCung = arr[index];
    }
  }
  return soChanCuoiCung;
}

//6.Handle Đổi vị trí
function handleDoiCho() {
  var viTri1 = document.querySelector("#txt-vi-tri-1").value * 1;
  var viTri2 = document.querySelector("#txt-vi-tri-2").value * 1;
  var viTriCanDoi = arrNum[viTri1];
  arrNum[viTri1] = arrNum[viTri2];
  arrNum[viTri2] = viTriCanDoi;
  document.querySelector(
    "#result-6"
  ).innerHTML = `<h3>Mảng sau khi đổi : ${arrNum}</h3>`;
}

//7.Handle Sắp xếp tăng dần
function handleSapXep() {
  arrNum.sort(soSanh);
  document.querySelector(
    "#result-7"
  ).innerHTML = `<h3> Mảng sau khi sắp xếp : ${arrNum}</h3>`;
}
function soSanh(a, b) {
  return a - b;
}

//8.Handle Tìm số nguyên tố đầu tiên
function testNumber(number) {
  var test = true;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      test = false;
      break;
    }
  }
  return test;
}

function handleTimSoNguyenToDauTien() {
  var ketQua = -1;
  for (var i = 1; i < arrNum.length; i++) {
    var test = testNumber(arrNum[i]);
    if (test) {
      ketQua = arrNum[i];
      break;
    } else {
      ketQua = -1;
    }
  }
  document.querySelector("#result-8").innerHTML=`<h3>Số Nguyên Tố đầu tiên : ${ketQua} </h3>`
}

//9.Handle Đếm số nguyên
//handle Tạo array mới, Thêm số vào array mới để kiểm tra số nguyên
var arrNumtt = [];
function themSott() {
  var numbertt = document.querySelector("#txt-number-tt").value * 1;
  arrNumtt.push(numbertt);
  document.querySelector("#txt-number-tt").value = "";
  document.querySelector("#Them-so-tt").innerHTML = `<h3> ${arrNumtt}</h3>`;
}

function handleDemSoNguyen() {
  document.querySelector(
    "#result-9"
  ).innerHTML = `<h3> Đếm số Nguyên : ${demSoNguyen(arrNumtt)}</h3>`;
}
function demSoNguyen(arrtt) {
  var demSoNguyen = 0;
  for (var index = 0; index < arrtt.length; index++) {
    if (Number.isInteger(arrtt[index])) {
      demSoNguyen++;
    }
  }
  return demSoNguyen;
}

//10.Handle so sánh số lượng số âm và số dương
function handleCompare() {
  if (demSoDuong(arrNum) > demSoAm(arrNum)) {
    document.querySelector(
      "#result-10"
    ).innerHTML = `<h3> Số dương > Số âm</h3>`;
  } else if (demSoDuong(arrNum) < demSoAm(arrNum)) {
    document.querySelector(
      "#result-10"
    ).innerHTML = `<h3> Số âm > Số dương</h3>`;
  } else {
    document.querySelector(
      "#result-10"
    ).innerHTML = `<h3> Số dương = Số âm</h3>`;
  }
}
function demSoAm(arr) {
  var demSoAm = 0;
  for (var index = 0; index < arr.length; index++) {
    var number = arr[index];
    if (number < 0) {
      demSoAm++;
    }
  }
  return demSoAm;
}
